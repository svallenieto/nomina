<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

**Instalación y Configuracion:**
~~~
composer install
~~~
~~~
php artisan key:generate
~~~
~~~
php artisan storage:link
~~~

**Configurar Base de Datos archivo .env:**
~~~
DB_DATABASE=data_base_name
DB_USERNAME=data_base_user
DB_PASSWORD=secret
~~~
~~~
php artisan migrate
~~~

**Instalar VUE :**

~~~
npm install
~~~
~~~
npm run dev
~~~

**Ejecutar entorno de desarrollo:**
~~~
php artisan serve
~~~


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
