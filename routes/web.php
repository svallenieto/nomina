<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\Dashboard\EmpleadosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');


require __DIR__.'/auth.php';



/**
 * Rutas de Usuarios
 */
Route::get('dashboard/empleados', [EmpleadosController::class, 'index'])
    ->middleware(['auth', 'verified'])->name('empleados');

Route::get('dashboard/create', [EmpleadosController::class, 'create'])
    ->middleware(['auth', 'verified'])->name('empleados.create');

Route::get('dashboard/empleados/{empleado}', [EmpleadosController::class, 'show'])
->middleware(['auth', 'verified'])->name('empleados.show');

Route::get('dashboard/empleados/edit/{empleado}', [EmpleadosController::class, 'edit'])
    ->middleware(['auth', 'verified'])->name('empleados.edit');

Route::post('dashboard/empleados', [EmpleadosController::class, 'store'])
->middleware(['auth', 'verified'])->name('empleados.store');

Route::put('dashboard/empleados/{empleado}', [EmpleadosController::class, 'update'])
->middleware(['auth', 'verified'])->name('empleados.update');

Route::put('dashboard/empleados/password/{empleado}', [EmpleadosController::class, 'updatePassword'])
    ->middleware(['auth', 'verified'])->name('empleados.password');
Route::get('dashboard/empleados/status/{empleado}/{estado}', [EmpleadosController::class, 'updateStatus'])
    ->middleware(['auth', 'verified'])->name('empleados.status');

Route::get('dashboard/empleados/detete/{empleado}', [EmpleadosController::class, 'destroy'])
->middleware(['auth', 'verified'])->name('empleados.delete');

