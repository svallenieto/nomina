<?php

namespace App\Http\Controllers\Dashboard;
use App\Models\Empleado;
Use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use phpDocumentor\Reflection\Types\Boolean;
use Ramsey\Uuid\Type\Integer;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = Empleado::all();
        Log::info('List Empleados: '.$empleados);
        /*return view('dashboard.users.home')
            ->with(compact('empleados'));;*/

        return Inertia::render('list', [
            'empleados' => $empleados
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'codigo' => 'required|string|max:255',
            'nombre' => 'required|string|max:255',
            'apellido_paterno' => 'required|string|max:255',
            'apellido_materno' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',

            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->nombre,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $empleado = $user->empleado()->create([
            'codigo' => $request->codigo,
            'nombre' => $request->nombre,
            'apellido_paterno' => $request->apellido_paterno,
            'apellido_materno' => $request->apellido_materno,
            'email' => $request->email,
            'tipo_contracto' => $request->tipo_contracto,
            'estado' => $request->estado
        ]);

        Log::info('Create Empleado: '.$empleado);


        return redirect()->route('empleados');
    }

    /**
     * Display the specified resource.
     *
     * @param  Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Empleado $empleado)
    {
        Log::info('Show Empleado: '.$empleado);
        /*return view('dashboard.users.show')
            ->with(compact('empleado'));*/

        return Inertia::render('show', [
            'empleado' => $empleado
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Empleado $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleado $empleado)
    {
        return Inertia::render('edit', [
            'empleado' => $empleado
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Empleado $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empleado $empleado)
    {
        $empleado->update([
            'codigo' => $request->codigo,
            'nombre' => $request->nombre,
            'apellido_paterno' => $request->apellido_paterno,
            'apellido_materno' => $request->apellido_materno,
            'tipo_contracto' => $request->tipo_contracto,
            'estado' => $request->estado
        ]);


        $user = User::find($empleado->user_id);
        $user->update(['name' => $request->nombre]);

        Log::info('Show User: '.$user);

        return Inertia::render('show', [
            'empleado' => $empleado
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Empleado $empleado
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, Empleado $empleado)
    {

        $request->validate([
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::find($empleado->user_id);
        $user->forceFill([
            'password' => Hash::make($request->password),
            'remember_token' => Str::random(60),
        ])->save();
        return Inertia::render('show', [
            'empleado' => $empleado
        ]);

        throw ValidationException::withMessages([
            'email' => [trans($status)],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $estado
     * @param  Empleado $empleado
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request, Empleado $empleado, $estado)
    {
        $empleado->update([
            'estado' => $estado
        ]);

        return redirect()->route('empleados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Empleado $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empleado $empleado)
    {
        $empleado->delete();
        return redirect()->route('empleados');
    }
}
